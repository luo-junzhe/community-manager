package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.entity.UserRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-01-27
 */
public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {

}
