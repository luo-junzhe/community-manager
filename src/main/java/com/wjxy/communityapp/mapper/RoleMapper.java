package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
public interface RoleMapper extends BaseMapper<RoleEntity> {
    /**
     * 通过用户账号 获取用户角色
     * @param loginAccount
     * @return
     */
    String findRoleByAccount(String loginAccount);

    /**
     * 通过角色名查询角色ID
     */
    Integer findRoleIDByName(String roleName);

}
