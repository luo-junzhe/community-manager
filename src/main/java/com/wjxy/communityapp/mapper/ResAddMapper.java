package com.wjxy.communityapp.mapper;


import com.wjxy.communityapp.entity.ResAddEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地址住户关联表 Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-01-23
 */
public interface ResAddMapper extends BaseMapper<ResAddEntity> {

}
