package com.wjxy.communityapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wjxy.communityapp.dto.RetrievePwdDto;
import com.wjxy.communityapp.dto.UserDto;
import com.wjxy.communityapp.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjxy.communityapp.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-05
 */
public interface UserService extends IService<UserEntity> {

     IPage<UserDto> queryUserByParam(Integer current,Integer limit,String keyword);

     Result  addUser(UserDto userDto);

     Result updateUser(UserDto userDto);

     Result deleteUser(Integer[] ids);

     UserDto queryUserDtoByAccount(String loginAccount);

     String updateUserPwd(String account,String oldPwd,String newPwd);

     Boolean updateUserInfo(UserDto userDto);

     Result sendSms(String account,String userPhone);
     //验证码验证
     Result checkVesCode(RetrievePwdDto retrievePwdDto);

     Result updateUserPwd(String account,String newPwd);

     Result registerUser(UserDto userDto);
}
