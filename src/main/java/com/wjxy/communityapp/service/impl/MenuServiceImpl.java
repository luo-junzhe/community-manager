package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.dto.MenuDTO;
import com.wjxy.communityapp.entity.MenuEntity;
import com.wjxy.communityapp.mapper.MenuMapper;
import com.wjxy.communityapp.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-03-30
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuEntity> implements MenuService {

    @Resource
    private MenuMapper menuMapper;

    @Cacheable(cacheNames = {"menus"})
    @Override
    public List<MenuDTO> listByUserId(Integer userId) {
        List<MenuDTO> list = menuMapper.listByUserId(userId);
        list = sortTree(list,-1);
        return list;
    }

    /**
     * 递归查询当前菜单的所有子菜单，并将所有的子菜单放到当前菜单的List集合中
     * @param list
     * @param parentId
     * @return
     */
    private List<MenuDTO> sortTree(List<MenuDTO> list, int parentId){
        ArrayList<MenuDTO> returnList = new ArrayList<>();
        for (MenuDTO dto : list) {
            if (parentId == dto.getParentId()){
                dto.setChild(sortTree(list,dto.getMenuId()));
                returnList.add(dto);
            }
        }
        return returnList;
    }


    /**
     * 排序相关代码
     * @param nlist
     */
    /*private void px(List<MenuDTO> nlist) {//排序
        for (MenuDTO menuDTO : nlist) {
            px(menuDTO.getList());
        }

        nlist.sort(new Comparator<MenuDTO>() {
            @Override
            public int compare(MenuDTO o1, MenuDTO o2) {
                return o1.getMenuId() - o2.getMenuId();
            }
        });
    }*/



}
