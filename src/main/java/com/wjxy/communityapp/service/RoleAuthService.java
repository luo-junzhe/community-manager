package com.wjxy.communityapp.service;

import com.wjxy.communityapp.dto.RoleAuthDto;
import com.wjxy.communityapp.entity.RoleAuthEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
public interface RoleAuthService extends IService<RoleAuthEntity> {

    List<RoleAuthDto> getAuthByRole(int roleId);
}
