package com.wjxy.communityapp.common;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @desc: 自定义JwtToken实现shiro的AuthenticationToken接口
 * @author: lc
 * @date: 2021/3/31
 */
public class JwtToken implements AuthenticationToken {

    private String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }
}
